from sympy import acos, solve_linear_system
from sympy.abc import x, y, z
from sympy.parsing.sympy_parser import parse_expr
from sympy.matrices import *

# Gives the set of fixed points of the affine transformation M.{x,y,z}+b
def fixedpoints(M, b):
	# Solve M.{x,y,z}+b=={x,y,z} -----------> (M-I)==-b
	sol = solve_linear_system((M - eye(M.cols)).row_join(-b), x, y, z)
	if sol == None:
		sol = {}
	return sol

# Gives a set of orthogonal vectors to the given vector v.
# G is the matrix of the inner product (Gramian matrix)
def orthogonal_vectors(G, v):
	return (v.transpose() * G).nullspace()

# Classifies the affine transformation M.{x,y,z}+b
# G is the matrix of the inner product (Gramian matrix)
def classify(G, M, b):
	# Routine checks
	if not G.is_square:
		print "Inner product matrix should be square!"
		return

	if not G.is_symmetric():
		print "Inner product matrix should be symmetric!"
		return

	if not all([m > 0 for m in G.berkowitz_minors()]):
		print "Inner product matrix should be positive definite!"
		return

	if not M.is_square:
		print "Matrix should be square!"
		return

	if b.rows != M.rows or b.cols != 1:
		print "Vector should be 1xN vector!"
		return

	n = M.cols

	# Check if the matrix is orthogonal
	ortho = (M.transpose() * G * M == G)
	print "Orthogonal:",ortho

	if not ortho:
		return

	# Check if it's a rotation or a symmetry
	det = M.det()
	print "Determinant:",det
	if det == 1:
		print "Class: Rotation"
	elif det == -1:
		print "Class: Symmetry"
	else:
		print "This shouldn't happen..."
		return

	if n == 2:
		if det == 1:
			""" There exists an orthonormal basis where the matrix is:
			    cos A | -sin A
			    sin A |  cos A
			"""
			# Find out rotation angle, based on the fact the trace is invariant for basis changes
			cosRotationAngle = M.trace() / 2
			print "Cosine of rotation angle:", cosRotationAngle
			print "Rotation Angle:", acos(cosRotationAngle)

			if cosRotationAngle == 1:
				# f(p) - p is the translation vector
				# We take p=(0,0) for simplicity
				p = zeros(n, 1)
				fp = M*p+b
				translationVector = (fp - p)

				print "Type: Identity matrix"
				print "Translation vector:"
				print translationVector
				return

			fixedPoint = fixedpoints(M, b)
			print "Type: Rotation around point"
			print "Fixed point:"
			print fixedPoint
			return

		if det == -1:
			""" There exists an orthonormal basis where the matrix is:
			     1 |  0
			     0 | -1
			"""
			fixedDirection = (M - eye(2)).nullspace()
			symmetryDirection = (M + eye(2)).nullspace()

			# Axis point: (p + f(p)) / 2 for all p
			# Translation vector: (f^2(p) - p) / 2 for all p
			# We take p=(0,0) for simplicity
			p = zeros(2, 1)
			fp = M*p+b
			ffp = M*fp+b
			axisPoint = (p + fp) / 2
			translationVector = (ffp - p) / 2

			print "Type: Axial symmetry (maybe followed of translation)"
			print "Axis point:"
			print axisPoint
			print "Axis direction:"
			print fixedDirection
			print "Symmetry direction:"
			print symmetryDirection
			print "Translation vector:"
			print translationVector
			return

	if n == 3:
		""" There exists an orthonormal basis where the matrix is:
		    cos A | -sin A |  0
		    sin A |  cos A |  0
		        0 |      0 | +-1 (= determinant value)
		"""

		# Find out rotation angle, based on the fact the trace is invariant for basis changes
		cosRotationAngle = (M.trace() - det) / 2
		print "Cosine of rotation angle:", cosRotationAngle
		print "Rotation Angle:", acos(cosRotationAngle)

		if det == 1:
			# Special case where all points are fixed points (identity matrix)
			if cosRotationAngle == 1:
				print "Type: Identity Matrix (Rotation of 0 degrees)"
				print "Translation vector:"
				print b
				return

			# We solve f(p)-p==(f(f(p))-p) / 2 for the points in the axis
			# Then we get a single point by setting {x,y,z}={0,0,0} for undetermined variables
			axis = solve_linear_system((2*M-eye(3)-M*M).row_join((M-eye(3))*b), x, y, z)
			axisPoint = Matrix([axis[s].subs({x:0,y:0,z:0}) if s in axis else 0 for s in [x,y,z]])

			axisDirection = (M - eye(3)).nullspace()

			# Since the axis is not affected by the rotation, f(p)-p is the translation vector
			translationVector = M*axisPoint+b-axisPoint

			rotationPlaneDirection = orthogonal_vectors(G, Matrix(axisDirection))

			print "Type: Rotation (maybe followed of translation)"
			print "Axis point:"
			print axisPoint
			print "Axis direction:"
			print axisDirection
			print "Axis points:"
			print axis
			print "Rotation plane directions:"
			print rotationPlaneDirection
			print "Translation vector:"
			print translationVector
			return
		elif det == -1:
			# Special case where there isn't a single fixed point
			if cosRotationAngle == 1:
				# Symmetry plane point: (p + f(p)) / 2 for all p
				# Translation vector: (f^2(p) - p) / 2 for all p
				# We take p=(0,0,0) for simplicity
				p = zeros(3, 1)
				fp = M*p+b
				ffp = M*fp+b
				symmetryPlanePoint = (p + fp) / 2
				translationVector = (ffp - p) / 2
				symmetryPlaneDirection = (M-eye(3)).nullspace()
				symmetryDirection = (M+eye(3)).nullspace()

				print "Type: Specular Symmetry (maybe followed of translation)"
				print "Symmetry plane point:"
				print symmetryPlanePoint
				print "Symmetry plane directions:"
				print symmetryPlaneDirection
				print "Symmetry direction:"
				print symmetryDirection
				print "Translation vector:"
				print translationVector
				return

			fixedPoint = fixedpoints(M, b)

			# Special case: Central symmetry (minus identity matrix)
			if cosRotationAngle == -1:
				print "Type: Central symmetry"
				print "Fixed point (center)"
				print fixedPoint
				return

			symmetryDirection_axisDirection = (M + eye(3)).nullspace()
			symmetryPlaneDirection = orthogonal_vectors(G, Matrix(symmetryDirection_axisDirection))

			print "Type: Rotation + Specular Symmetry"
			print "Fixed point (from axis and symmetry plane):"
			print fixedPoint
			print "Symmetry plane directions:"
			print symmetryPlaneDirection
			print "Rotation axis direction / Symmetry direction:"
			print symmetryDirection_axisDirection
			return

"""
from sympy import sqrt, Rational

M = Matrix([[sqrt(3)/2, -Rational(1, 2)], [Rational(1, 2), sqrt(3)/2]])
b = Matrix([[1], [1]])
classify(eye(2), M, b)
print

M = Matrix([[5, 6, sqrt(3)], [-6, 4, 2*sqrt(3)], [sqrt(3), -2*sqrt(3), 7]])/8
b = Matrix([[2], [0], [2*sqrt(3)]])
classify(eye(3), M, b)
print

M = Matrix([[2,-3,6],[3,6,2],[6,-2,-3]])/7
b = Matrix([[2],[2],[2]])
classify(eye(3), M, b)
print

G = Matrix([[1,-1,0],[-1,2,-Rational(1,2)],[0,-Rational(1,2),3]])
M = Matrix([[-1,0,1],[0,-1,1],[0,0,1]])
b = Matrix([[0],[0],[1]])
classify(G, M, b)
print
"""

n = input("Matrix size: ")
G = [[0 for j in range(n)] for i in range(n)]
M = [[0 for j in range(n)] for i in range(n)]
b = [[0] for i in range(n)]

print "Inner product matrix:"
for i in range(n):
	for j in range(n):
		G[i][j] = parse_expr(raw_input(str(i)+","+str(j)+": "))

print "Affine transform matrix:"
for i in range(n):
	for j in range(n):
		M[i][j] = parse_expr(raw_input(str(i)+","+str(j)+": "))

print "Affine transform vector:"
for i in range(n):
	b[i] = parse_expr(raw_input(str(i)+": "))

classify(Matrix(G), Matrix(M), Matrix(b))